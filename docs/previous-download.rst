..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _tf-archives:

:orphan:

###########################
SKF archives
###########################

.. contents::
   :local:
   :backlinks: top

Squash Keyword Framework
========================

**Latest Version : 1.14.0**

  * :download:`Squash Keyword Framework 1.14.0 Release Note <_static/release-note/keyword-framework/squash-tf-keyword-framework-1.14.0.md>`

Previous Versions :

* 1.13.0

  * :download:`Squash Keyword Framework 1.13.0 Release Note <_static/release-note/keyword-framework/squash-tf-keyword-framework-1.13.0.md>`

* 1.12.0

  * :download:`Squash Keyword Framework 1.12.0 Release Note <_static/release-note/keyword-framework/squash-tf-keyword-framework-1.12.0.md>`

* 1.11.0

  * :download:`Squash Keyword Framework 1.11.0 Release Note <_static/release-note/keyword-framework/squash-tf-keyword-framework-1.11.0.md>`

* 1.10.1

  * :download:`TA framework 1.10.1 Release Note <_static/release-note/keyword-framework/ta-framework-1.10.1.pdf>`

* 1.9.0

  * :download:`TA framework 1.9.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.9.0.md>`

* 1.7.3

  * :download:`TA framework 1.7.3 Release Note <_static/release-note/keyword-framework/ta-framework-1.7.3.md>`

* 1.7.2

  * :download:`TA framework 1.7.2 Release Note <_static/release-note/keyword-framework/ta-framework-1.7.2.md>`

* 1.7.0

  * :download:`TA framework 1.7.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.7.0.md>`

* 1.6.0

  * :download:`TA framework 1.6.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.6.0.md>`

* 1.5.0

  * :download:`TA framework 1.5.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.5.0.md>`

* 1.4.0

  * :download:`TA framework 1.4.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.4.0.md>`

* 1.3.0

  * :download:`TA framework 1.3.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.3.0.md>`


Eclipse toolbox
===============

**Latest Version : 1.10.0**

* :download:`Eclipse toolbox 1.10.0 Release Note <_static/release-note/toolbox-eclipse/toolbox-eclipse-1.10.0.md>`
* `squash-ta-tools-bundle-1.10.0-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/squash-ta-tools-bundle-1.10.0-RELEASE-installer.exe.jar>`_

Previous Versions :

* 1.9.1

  * `squash-ta-tools-bundle-1.9.1-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.9.1-RELEASE-installer.exe.jar>`__

* 1.9.0

  * :download:`Eclipse toolbox 1.9.0 Release Note <_static/release-note/toolbox-eclipse/ta-toolbox-1.9.0.md>`
  * `squash-ta-tools-bundle-1.9.0-RELEASE-installer.exe.jar> <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.9.0-RELEASE-installer.exe.jar>`__

* 1.7.1

  * `squash-ta-tools-bundle-1.7.1-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.7.1-RELEASE-installer.exe.jar>`__

* 1.7.0

  * :download:`Eclipse toolbox 1.7.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.7.0.md>`
  * `squash-ta-tools-bundle-1.7.0-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.7.0-RELEASE-installer.exe.jar>`__

* 1.6.0

  * `squash-ta-tools-bundle-1.6.0-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.6.0-RELEASE-installer.exe.jar>`__

* 1.5.0

  * :download:`TA framework 1.5.0 Release Note <_static/release-note/keyword-framework/ta-framework-1.5.0.md>`
  * `squash-ta-tools-bundle-1.5.0-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.5.0-RELEASE-installer.exe.jar>`__

* 1.4.0

  * `squash-ta-tools-bundle-1.4.0-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/archives/squash-ta-tools-bundle-1.4.0-RELEASE-installer.exe.jar>`__


Eclipse plugin
==============

**Latest Version : 1.2.1**

* :download:`eclipse plugin Release Note <_static/release-note/toolbox-eclipse/ta-eclipse-plugin-1.2.1.20140326-1001.md>`
* `org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.2.1.20140326-1001.zip <http://repo.squashtest.org/distribution/org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.2.1.20140326-1001.zip>`__

Previous Versions :

* 1.2.0 :

  * :download:`Release Note <_static/release-note/toolbox-eclipse/ta-eclipse-plugin-1.2.0.md>`
  * `org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.2.0.20130625-1435.zip <http://repo.squashtest.org/distribution/archives/org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.2.0.20130625-1435.zip>`__

* 1.1.0 :

  * `org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.1.0.20130328-1130.zip <http://repo.squashtest.org/distribution/archives/org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.1.0.20130328-1130.zip>`__

* 1.0.0 :

  * :download:`Release Note <_static/release-note/toolbox-eclipse/ta-eclipse-plugin-1.0.0.md>`
  * `org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.0.0.20121220-1020.zip <http://repo.squashtest.org/distribution/archives/org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.0.0.20121220-1020.zip>`__



Intellij plugin
===============

**Latest Version : 1.0.0**

  * :download:`Intellij plugin 1.0.0 Release Note <_static/release-note/intellij-plugin/squash-tf-intellij-plugin-1.0.0.md>`
  * `squash-tf-intellij-plugin-1.0.0-RELEASE.zip <http://repo.squashtest.org/distribution/squash-tf-intellij-plugin-1.0.0-RELEASE.zip>`__

Previous Versions :

* 0.0.2

  * :download:`Intellij plugin 0.0.2 Release Note <_static/release-note/intellij-plugin/squash-tf-intellij-plugin-0.0.2.md>`
  * `squash-tf-intellij-plugin-0.0.2-RELEASE.zip <http://repo.squashtest.org/distribution/squash-tf-intellij-plugin-0.0.2-RELEASE.zip>`__

* 0.0.1

  * :download:`Intellij plugin 0.0.1 Release Note <_static/release-note/intellij-plugin/squash-tf-intellij-plugin-0.0.1.md>`
  * `squash-tf-intellij-plugin-0.0.1-RELEASE.zip <http://repo.squashtest.org/distribution/squash-tf-intellij-plugin-0.0.1-RELEASE.zip>`__

------------

Archive Repository
==================

For all bundles not listed here, you should found them in our `Archive repository <http://repo.squashtest.org/distribution/archives/>`__
