..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################
SKF Download Page
#################

Squash Keyword Framework
========================

**Latest Version : 1.14.0**

* :download:`Squash Keyword Framework 1.14.0 Release Note <_static/release-note/keyword-framework/squash-tf-keyword-framework-1.14.0.md>`

Squash Eclipse Toolbox
======================

**Latest Version : 1.10.0**

* :download:`TA toolbox 1.10.0 Release Note <_static/release-note/toolbox-eclipse/toolbox-eclipse-1.10.0.md>`

* `squash-ta-tools-bundle-1.10.0-RELEASE-installer.exe.jar <http://repo.squashtest.org/distribution/squash-ta-tools-bundle-1.10.0-RELEASE-installer.exe.jar>`_

Squash Eclipse Plugin
=====================

**Latest Version : 1.2.1**

* :download:`eclipse plugin Release Note <_static/release-note/toolbox-eclipse/ta-eclipse-plugin-1.2.1.20140326-1001.md>`
* `org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.2.1.20140326-1001.zip <http://repo.squashtest.org/distribution/org.squashtest.ta.eclipse.squash-ta-eclipse-repository-1.2.1.20140326-1001.zip>`__

Squash IntelliJ IDEA Plugin
===========================

**Latest Version : 1.0.0**

* :download:`IntelliJ IDEA plugin Release Note <_static/release-note/intellij-plugin/squash-tf-intellij-plugin-1.0.0.md>`
* `squash-tf-intellij-plugin-1.0.0-RELEASE.zip <http://repo.squashtest.org/distribution/squash-tf-intellij-plugin-1.0.0-RELEASE.zip>`__

Notepad++ Squash UDL
=====================

.. _notepadpp_squash_udl:

Contribution from **edegenetais**.

**Latest Version : 1.1**

* :download:`Notepad ++ Squash TF UDL  <_static/notepadpp/squashTA-UDL-1.1.xml>`




Archives
========

:ref:`Archives <tf-archives>`
