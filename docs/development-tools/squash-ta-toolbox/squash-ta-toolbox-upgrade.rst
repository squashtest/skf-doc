..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


#########################
Eclipse Toolbox - Upgrade
#########################

.. contents:: Contents:
    :local:

* Download the new Squash TA toolbox you want to install `here <https://www.squashtest.com/telechargements>`_.

* Install the new toolbox version in a separate directory as classical new install (For more information, please consult this :ref:`page<eclipse.toolbox.install anchor>`)

* You are ready to work. 


**Remark:**

 * Be careful, if you had special configurations for your project in your previous toolbox, then they will be lost. You had to set it again. You could use the eclipse import existing project feature, but for maven multimodule projects this doesn't work.
 * **For 1.9.0** : If you are migrating to version 1.9.0 of Squash-TA-Framework, the logging has been upgraded to log4j 2-5. If you want to use project developped with older version of TA you need to modify your log4j configuration files according to `Apache log4j documentation <https://logging.apache.org/log4j/2.x/manual/migration.html>`_.

