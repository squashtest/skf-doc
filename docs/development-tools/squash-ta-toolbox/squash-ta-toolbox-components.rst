..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


############################
Eclipse Toolbox - Components
############################

.. contents:: Contents:
    :local:

The Squash-TA toolbox 1.10.0 contains the following tools: 

    * Maven 3.5.0, 
    * Sahi v51,
    * Jailer 7.3.1,
    * Eclipse Oxygen 64-bits as well as a preconfigured workspace

        * m2e
        * Squash TA eclipse plugin
        * "Run configurations".
    * An uninstall assistant

The version 1.9.0 contains:

    * Maven 3.0.4 (for compatibility with framework versions before1.9.0),
    * A patched version of Maven 3.3.3 (you can find here details about the patch),  
    * Sahi,
    * Selenium-server,
    * Jailer,
    * Eclipse Mars as well as a preconfigured workspace

        * m2e
        * Squash TA eclipse plugin
        * "Run configurations" and eclipse preferences to import.
    * An uninstall assistant


Before the 1.9.0 version the toolbox used to contain: 

    * Maven 3.0.4,
    * Sahi,
    * Selenium-server,
    * Jailer,
    * Eclipse Helios as well as a preconfigured workspace

        * m2e
        * Squash TA eclipse plugin
        * "Run configurations" and eclipse preferences to import.
    * An uninstall assistant
    * open-jdk 6.
