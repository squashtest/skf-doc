..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _eclipse.toolbox.install anchor:

#########################
Install Squash-TA Toolbox
#########################

.. contents:: Contents:
    :local:


**Prerequisite** : Before starting the installation of SQUASH-TA Toolbox, you must install a JDK between 1.7 and 1.8, both 32 and 64 bits version are compatible. See `here <https://www.oracle.com/technetwork/java/javase/downloads/index.html>`_ to download it from Oracle website.

First of all, download the toolbox installer a the the following `link <https://www.squashtest.com/telechargements>`_.

    * Launch the installer by double-clicking on it:

    * On the first screen, click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-1.png

    * You will then be prompted to indicate the path of your JDK (to do so, click on the **Browse** button. Click on **Next** when it is done:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-2.png

    * On the next screen, you will see all the licenses of the different components of Squash TA Toolbox. Click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-3.png

    * The next step allows you choose your toolbox install location. You can directly type the path in the input field, or click on **Browse** and select your chosen location (you may create your installation folder from there if needed). After choosing the location, click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-4.png

    * If the target folder already exists (even if you've just created it), you will get the following alertbox. Click on **Yes** to confirm the location:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-5.png

    * Next, you will have to choose a location to create your new Eclipse workspace for your Squash-TF projects. As for the install location, you may type the path directly or use **Browse** button. If the location you typed does not exist, it will be automatically created. After choosing your location, click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-6.png

    * If the location already exists, you will get the following error message. You can't create the Eclipse workspace in an existing location:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-7.png

    * The next step lets you check the list of installed components. You may reuse some compatible components already installed on your workstation but for now let's assume that you will be getting all tools from the toolbox, and leave all boxes checked. Click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-8.png

    * You can choose the shortcuts you want to create for which users. To install the tools for yourself only, check **current user** on the right radio group. Click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-9.png

    * During this step, installation files are copied to the selected location. When it's done, click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-10.png

    * Some more information about the installation. Click on **Next**:

        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-11.png

    * Done! You have just installed your Squash-TF test automation environment:

       
        .. figure:: ../../_static/eclipse-toolbox/eclipse-toolbox-install-12.png
