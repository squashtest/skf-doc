..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


#################
Squash-TA Toolbox
#################

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Components of Squash-TA toolbox<squash-ta-toolbox-components.rst>
    Install Squash-TA Toolbox<squash-ta-toolbox-install.rst>
    Upgrade Squash-TA Toolbox<squash-ta-toolbox-upgrade.rst>
    Eclipse "Run configuration"<squash-ta-toolbox-runconf.rst>

The Squash-TA toolbox is a package which includes all necessary tools to realize automated tests scripts. Apart from the SKF, this package includes Open Source tools as Eclipse, Jailer (For the creation of data sets), Selenium, Sahi.
This section describe the components of the Squash TA toolbox and explains how to install and how to upgrade it. 

