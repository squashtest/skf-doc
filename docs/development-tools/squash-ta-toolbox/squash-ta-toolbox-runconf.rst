..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###################################
Eclipse "Run configuration" for SKF
###################################

Squash TA toolbox default workspace embeds some default eclipse "Run configuration" to run Squash Keyword Framework / Squash TA automation project. We will describe them below.
Those eclipse **"Run configuration" are part of the workspace and not of the project**. So we will see in second time how to import them.


*****************************************
"Run configuration" in Squash-TA toolbox
*****************************************

If you have installed the Squash-TA Toolbox and that you are using Squash-TA Eclipse, go to 'Run/Run configurations':

.. figure:: ../../_static/eclipse-toolbox/run-configuration.jpg

There, you have the pre-installed maven run configurations:

.. figure:: ../../_static/eclipse-toolbox/run-configuration2.png

Since Squash-TA toolbox 1.9.0, there is three sets of run configuration:

    * One set which use the goal "squash-tf:run" to run the test and specify location of lo4j configuration file "-Dlog4j.configurationFile=log4j2.xml" 
  
        * Run all tests
    
        * Run selected test(s)
    
        * Run test list
    
    * One set which use the maven lifecycle "squash-tf:run" to run the test
  
        * (Before TA fwk 1.9.0) Run all tests
    
        * (Before TA fwk 1.9.0) Run selected test(s)
    
        * (Before TA fwk 1.9.0) Run test list
    
    * One set which use the maven lifecycle "integration-test" to run the test
  
        * (Before TA fwk 1.7.0) Run all tests
    
        * (Before TA fwk 1.7.0) Run selected test(s)
    
        * (Before TA fwk 1.7.0) Run test list


.. note::

  To run older projects (< 1.9.0), you must use the prefixed run configuration..


**Differences between all Run configuration:**

.. list-table::

  * - Default Config
    - squash-ta:run (+log4j)
        * - Before TA Framework 1.6.0
          - squash-ta:run
  * - Before TA Framework 1.7.0
    - integration-test

|

******************************************************
Import eclipse "Run configuration" for SKF / Squash-TA
******************************************************

To import run configuration in eclipse

# Download the "Run configurations" `here <https://bitbucket.org/nx/squash-ta-toolbox/src/squash-ta-toolbox-umbrella-1.10.0-RELEASE/squash-ta-tools-bundle/src/main/resources/workspace-metadata/.plugins/org.eclipse.debug.core/.launches/>`_

# In eclipse open : `File\Import`

  ..figure :: ../../_static/eclipse-toolbox/import.png

# In the dialbox, choose `Run/debug` then `Launch Configurations` and click on `:guilabel: Next`

# Click on `:guilabel: Browse...` and pick the directory where "Run configurations" were download at the first step.

# Select the "Run configurations" you want to import then click on `:guilabel: Finish`


