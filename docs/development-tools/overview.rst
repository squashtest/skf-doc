..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _development-tools-overview:

####################
Our development tool
####################

.. toctree::

   eclipse-tools.rst
   Intellij Plugin <https://skf.readthedocs.io/projects/intellij-plugin/>
   other-ide.rst

.. note:: The old wiki is currently still available here : `<https://sites.google.com/a/henix.fr/wiki-squash-ta/user>`_
