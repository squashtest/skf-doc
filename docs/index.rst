..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _squash_tf_doc_home:

#######################################################
Welcome to Squash Keyword Framework (SKF) documentation
#######################################################

.. toctree::
   :maxdepth: 2
   :hidden:

   < Squash TF Doc Portal > <https://squash-tf-portal.readthedocs.io>
   SKF <https://skf.readthedocs.io/projects/skf/en/doc-stable>
   IDE <development-tools/overview.rst>
   Download <download.rst>
   Community <developer/community.rst>

**S**\ quash **K**\ eyword **F**\ ramework (**SKF**) is keyword oriented test automation framework using, most of the time, existing Robots.
This framework includes a DSL for writing automated tests scripts and an engine to execute the scripts. It allows the use of multiple robots in one single test.

.. warning::

    SKF is currently not a part of Squash TF roadmap and therefore no new developments are expected.

======================
Keyword test framework
======================

**In a keyword tests framework :**

* test scripts are a composition of keywords
* keywords are small blocks of reusable features
* the framework provides a library of default keyword.

**In Squash Keyword Framework :**

* keywords are made of macros
* a large set of built-in macros is provided
* macros can be combined to create a higher level macro
* macros could also be created by using low level instructions, if needed

===================
Engine with plugins
===================

At the heart of our framework, there is an engine which using plugins to pilot test robots. For each robot, a plugin (connector) has been created which provides :

* the built-in macro
* the low level instructions and their implementation (to pilot the robot).

The Squash Keyword Framework plugin architecture makes it an expandable solution : you can contribute by creating a new plugin for a new robot (or by extending an existing one).
See community section for contribution.
