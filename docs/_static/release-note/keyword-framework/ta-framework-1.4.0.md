
Squash TA - 1.4.0    
==========================================                                                                     

Enhancement
-----------
- 0001576: [Enhancement] Add integration test for SQL parametered requests
- 0001538: [Enhancement] Update the archetype POM template to avoid adding events to standalone builds, which don't require them
- 0001516: [Enhancement] [FTP] Delete file if exists
- 0001488: [Enhancement] Difficulty to identify the cuplrit test when an error syntax is detected
- 0001482: [Enhancement] [SSH] : all tests executions in build failure if a ssh target is configured but server connection failed
- 0001443: [Enhancement] [Shortcut] Add a delete_dbunit macro
- 0001442: [Enhancement] [Shortcut] Insert_dbunit macro should use "insert" operation rather than "clean_insert"
- 0001361: [Enhancement] Add integration test for FTP shortcuts
- 0001158: [Enhancement] [Squash-TA toolbox] Remove unused m2eclipse configuration from the toolbox workspace
- 0001014: [Enhancement] [squash-ta-maven-plugin] Use a wildcards to indicate which tests to run

Issue
-----
- 0001637: [Issue] [DbUnit]In some case DbUnit assert contains malfunctionned
- 0001546: [Issue] Typo in the start.sh script of the Squash TA execution server for linux
- 0001541: [Issue] In ecosystem scripts all instruction with configuration resources fail with ResourceNotFoundException
- 0001512: [Issue] two SLF4j implementations (through squash-ta-link)
- 0001511: [Issue] Workspace installation fails with a ClassDefNotFoundError
- 0001507: [Issue] [squash-ta-maven-plugin] The Maven build freezes during test compilation when the test path contains a special character
- 0001504: [Issue] [Squash-TA toolbox] Run configurations refer to the wrong JRE container
- 0001497: [Issue] Freezing of selenium servers if output buffers full
- 0001495: [Issue] Specificity bug in the macro matching algorithm
- 0001494: [Issue] Missing information on how to customize browser settings in the execution server sahi instance
- 0001485: [Issue] The default configuration produced by the archetype does not allow standalone execution
- 0001484: [Issue] Grossly incorrect diff report on ASSERT contain Dbunit
- 0001337: [Issue] [SoapUI] "Groovy script" test steps fail due to groovy/lang/GroovyClassLoader missing
- 0001152: [Issue] [SoapUI] SoapUI runner abusively resets logging configuration
- 0000960: [Issue] [Shortcuts] Instability of the macro mechanisme due to non-deterministic variable name generation
- 0000843: [Issue] [DbUnit] Permettre de renseigner plusieurs pseudo-clefs primaires par table (= pseudo clefs primaires composites)
- 0001485: [Issue] The default configuration produced by the archetype does not allow standalone execution

Task
----
- 0001748: [Task] Accept script setup.ta and teardown.ta as setup and teardown ecosystem script