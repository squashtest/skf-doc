
SquashTA - 1.3.0
=======================================
- 0001400: [Enhancement] [SoapUI] Define the testsuite and testcase names inline, rather than in a property file (edegenetais) - resolved.
- 0001228: [Enhancement] [Filechecker] Text files encoding is not taken into account (edegenetais) - resolved.
- 0000931: [Enhancement] [Dbunit filter] table inclusions and exclusions should have similar formats (bfranchet) - closed.
- 0001319: [Enhancement] [Shortcut] Add shortcuts for FTP operations (bfranchet) - closed.
- 0001221: [Enhancement] [Filechecker] "id" fields do not need to be validated (bfranchet) - closed.
- 0001295: [Enhancement] [Filechecker] FFF descriptor validation should ensure that no binary specific field is used for a text file (bfranchet) - closed.
- 0001364: [Enhancement] [Filechecker] Improve error message when a record cannot be identified (bfranchet) - closed.
- 0000862: [Enhancement] [Lanceur de batch SSH] Exécution de batches via une commande SSH (edegenetais) - closed.
- 0001399: [Enhancement] [Shortcuts] Add shortcuts for standard stream assertions (bfranchet) - closed.
- 0001391: [Enhancement] [Shortcuts] Change definition of macro "execute_ssh_command_with_timeout.macro" (edegenetais) - closed.
- 0000860: [Enhancement] [SoapUI] Intégration de SoapUI pour les tests de services SOAP (edegenetais) - closed.
- 0001164: [Enhancement] [Filechecker] Replace "libelle" with "label" in the FFF descriptor (bfranchet) - closed.
- 0001218: [Enhancement] [Selenium] Execute Selenium HTML testcases with Squash-TA (edegenetais) - resolved.
- 0000953: [Enhancement] [Selenium] Modify Squash-TA lifecycle in order to start the selenium server in the ecosystem setup phase (edegenetais) - resolved.
- 0001015: [Enhancement] [squash-ta-maven-plugin] Use regular exressions to indicate which tests to run (edegenetais) - resolved.
- 0000917: [Enhancement] [Squash-TA Toolbox] configurer Eclipse pour qu'il utilise le maven de la toolbox plutôt que celui d'eclipse (bfranchet) - closed.
- 0001157: [Enhancement] add macro assert-equal to common components (edegenetais) - closed.
- 0001030: [Enhancement] [Squash-TA Toolbox] Replace oracle distributed jdk by an open jdk (edegenetais) - closed.
- 0000864: [Enhancement] [FTP] Création de répertoires lors du dépôt de fichier sur une cible FTP (bfranchet) - closed.
- 0001010: [Enhancement] [Shortcuts] Create shortcut to execute and assert success of SSH command (edegenetais) - closed.
- 0001013: [Issue] [Shortcuts] the macro expander leaks temporary files (bfranchet) - closed.
- 0001077: [Issue] the "Run selected test" run configuration does not work if the test is part of a non-default ecosystem (edegenetais) - resolved.
- 0001054: [Issue] the reporting system leaks temporary files because the result object implementation has a noop cleanUp implementation (edegenetais) - resolved.
- 0001061: [Issue] The DEFINE micro instruction runner in the engine adds an EOL sequence to the defined resource (edegenetais) - resolved.