MIGRATION GUIDE from Squash TA 1.4.0 to Squash TA 1.5.0
=======================================================
To migrate a test project from Squash TA 1.4.0 to Squash TA 1.5.0, you have to do some modification in the pom.xml file:
- Change the version of the artifact squash-ta-maven- plugin and squash-ta-link to the new 1.5.0-RELEASE. If you reference plugin artifact (squash-ta-commons-component, squash-ta-filechecker, squash-ta-sahi, squash-ta-selenium, squash-ta-soapui)
  - Change their groupId from org.squashtest.ta to org.squashtest.ta.plugin
  - Change their artifactId :
    - squash-ta-commons-component  ? squash-ta-plugin-commons-component
    - squash-ta-filechecker ? squash-ta-plugin-filechecker
    - squash-ta-sahi ? squash-ta-plugin-sahi
    - squash-ta-selenium ? squash-ta-plugin-selenium
    - squash-ta-soapui ? squash-ta-plugin-soapui
  - If you reference squash-ta-commons-component / squash-ta-plugin-commons-component, some more modification could be needed ( as squash-ta-plugin-commons-component has been split into squash-ta-plugin-commons-component, squash-ta-plugin-db, squash-ta-plugin-ftp, squash-ta-plugin-ssh).

In your Test script, you should add the �TEST :� phase marker if they not already have it. If you keep the sample TA test script provide with the archetype in the previous version (), then you have to add the �TEST :� phase marker to this script.



Squash TA - 1.5.0.RELEASE
=========================
- 0001998: [[TA] Enhancement] [Macros] DbUnit macros with ppk filter - Closed.
- 0001921: [[TA] Enhancement] [DbUnit assert equals] Change the behaviour of "assert equals" assertion - Closed.
- 0001934: [[TA] Enhancement] Command to write into the logs - Closed.
- 0001592: [[TA] Enhancement] Modify existing macro in order to use randomisation only one time for each variable - Closed.
- 0001873: [[TA] Enhancement] Change all the copyright dates into "2013" - Closed.
- 0002087: [[TA] Enhancement] [SFTP] Add integration tests - Closed.
- 0002052: [[TA] Enhancement] Add a log4j configuration file in the squash-ta-maven-project-archetype - Closed.
- 0002043: [[TA] Enhancement] [Sahi/Shortcuts] EXECUTE_SAHI macro should use param.relativedate converter - Closed.
- 0001559: [[TA] Enhancement] [SFTP] Execute SFTP operations - Closed.
- 0001844: [[TA] Enhancement] [TA commons component] Instruction to insert a pause in a test - Closed.
- 0001620: [[TA] Enhancement] [SQL query parameters] Log the SQL query after replacing its parameters with values - Closed.
- 0001614: [[TA] Enhancement] [Sahi/Shortcuts] EXECUTE_SAHI_BUNDLE macro should use param.relativedate converter - Closed.
- 0001617: [[TA] Enhancement] [SSH/Shortcuts] Macro to execute a command from a file using SSH - Closed.
- 0001621: [[TA] Enhancement] [Shortcuts] New macro : delete dbunit using PPK filter - Closed.
- 0001098: [[TA] Enhancement] [Command Sahi] Trim property from sahiConf.properties - Closed.
- 0001943: [[TA] Enhancement] [squash-ta-framework] Create a VoidResource for the commands, which use no resource and/or return nothing - Closed.
- 0001089: [[TA] Enhancement] [IT] integration test target plugin version switch is currently manual dureing the release process - Closed.
- 0001551: [[TA] Enhancement] [Target definition] Use shebangs to recognize the database and FTP target property files - Closed.
- 0001994: [[TA] Enhancement] [Ta selenium plugin] Upgrade selennium version to 2.31.0 - Closed.
- 0001992: [[TA] Enhancement] [sahi plugin]  Upgrade sahi version to v43_20130206 - Closed.
- 0001562: [[TA] Enhancement] [Filechecker] Assertions on the content of a FileResource - Closed.
- 0001945: [[TA] Enhancement] [squash-ta-db] Upgrade to DbUnit 2.4.9 - Closed.
- 0001240: [[TA] Enhancement] [squash-ta-maven-plugin] Create an umbrella dependency for all engine component dependencies - Closed.
- 0001459: [[TA] Enhancement] [Refactoring] Split commons-components module into commons-components and plugins - Closed.
- 0000391: [[TA] Enhancement] [DbUnit] pseudo clefs primaires : renseigner des pseudo PK uniquement pour les tables qui n'ont pas de PK - Closed.
- 0001753: [[TA] Enhancement] Separate TA distribution part from TA framework part - Closed.
- 0001622: [[TA] Enhancement] [FileToSQLNamedParameters] Syntax of the parameters should be "key=value" rather than "key,value" - Closed.
- 0001960: [[TA] Enhancement] [FTP] : uploading binary resources to a FTP server with the filetype set to "ascii" should trigger a build failure - Closed.
- 0001890: [[TA] Enhancement] [SSH] : some ssh related error messages are pretty imprecise - Closed.
- 0001516: [[TA] Enhancement] [FTP] Delete file if exists - Closed.
- 0001851: [[TA] Enhancement] Upgrade selenium lib to version 2.31.0 - Closed.
- 0002049: [[TA] Issue] Using closing parenthesis failed to convert inline resource - Closed.
- 0001574: [[TA] Issue] [RelativeDateConverter] The relative date converter fails when converting directories - Closed.
- 0002148: [[TA] Issue] [DbUnit] Assert contain : error instead of failure in the surefire report (AND no diff-report) - Closed.
- 0001632: [[TA] Issue] [DbUnit] AssertDbUnitEquals does not use the pseudo primary keys - Closed.
- 0002103: [[TA] Issue] The failures and errors occurring during the testcases setup and teardown phases should be logged, with an 'error' level - Closed.
- 0001889: [[TA] Issue] [SQL QUERY] : a SQL query file can contain several queries - Closed.
- 0001525: [[TA] Issue] [DEFINITION RESSOURCE EN LIGNE] : La présence de parenthèses dans une requête SQL empêche sa validation - Closed.
- 0001507: [[TA] Issue] [squash-ta-maven-plugin] The Maven build freezes during test compilation when the test path contains a special character - Closed.
- 0001823: [[TA] Issue] Error trace only show the last error found - Closed.
- 0002060: [[TA] Issue] [squash-ta-maven-plugin] squash-ta:test-list does not display the test names - Closed.
- 0001615: [[TA] Issue] [CommandLineResource] The command generated by the CommandLineResource should include all the instructions from the command file - Closed.
- 0001747: [[TA] Issue] Update TA source as .ta has been choose as default TA script extension - Closed.
- 0001499: [[TA] Issue] [Automation Project] TA script with no TEST marker - Closed.
- 0001905: [[TA] Issue] [squash-ta-sahi-plugin] The test should fail if the browser configured in squash-ta doesn't match any browser configured in Sahi - Closed.
- 0001954: [[TA] Issue] [FTP] :  corrupted zip files after an upload to a FTP server - Closed.
- 0001956: [[TA] Issue] [FTP] : corrupted files when uploaded to a FTP server with the property "filetype" set to "binary" - Closed.
- 0001941: [[TA] Issue] [SQL SHORTCUT] : wrong error message when trying to load a nonexistent resource from the root path of the "resources" folder - Closed.
- 0001500: [[TA] Issue] [Automation Project] TA script with 2 TEARDOWN markers - Closed.
- 0001501: [[TA] Issue] [Automation Project] TA script with no SETUP phase - Closed.
- 0001826: [[TA] Issue] [squash-ta-functional-tests] Ecosystem run automatically runs F02.Automation_Project/KO/Cas_de_tests - Closed.
- 0001763: [[TA] Issue] [setup] Resource not found in setup phase - Closed.
- 0002112: [[TA] Issue] Wrong error message when an instruction contains erroneous tokens - Closed.
- 0002035: [[TA] Issue] [log command] NullPointerException when trying to write an empty message - Closed.
- 0002094: [[TA] Issue] [SSHTargetCreator] Change log level to WARN when a error is detected in a SSH target configuration file - Closed.
- 0002051: [[TA] Issue] Modify default log4j configuration - Closed.
- 0002067: [[TA] Issue] ResourceNotFoundException : Error message should include the erroneous instruction. - Closed.
- 0002055: [[TA] Issue] Squash-TA engine should not try to instantiate .keep files located in targets or repositories directories - Closed.
- 0002099: [[TA] Issue] [InstructionRuntimeException] Improve error message - Closed.
- 0002070: [[TA] Issue] [DbUnit assert contains] Improve the error message when the "contained" dataset has more tables than the "containing" one - Closed.
- 0002104: [[TA] Issue] [DatabaseTargetCreator] Should use an 'error' log level when a mandatory property is missing - Closed.
- 0002072: [[TA] Issue] [Assert DbUnit contains] Typo in error message - Closed.
- 0002105: [[TA] Issue] [WebTargetCreator] Change log level to error when a mandatory property is missing - Closed.
- 0001224: [[TA] Issue] Throw an exception when a resource file name contains invalid characters - Closed.
- 0002057: [[TA] Issue] The test does not fail when there is a comment at the end of a instruction (on the same line) - Closed.
- 0001522: [[TA] Issue] FTP delete command is supposed to return a FileResource, but returns null - Closed.
- 0001852: [[TA] Issue] Add FTP passive mode - Closed.
- 0001436: [[TA] Issue] [framework, plugin dev] plus d'annotations pour les components - Closed.
- 0001749: [[TA] Issue] Update integration test  and unit test to the use of .ta extension - Closed.
- 0001989: [[TA] Issue] Change soapui maven repository URL - Closed.
- 0001990: [[TA] Issue] [project-archetype] Add token "TEST :" - Closed.
- 0001930: [[TA] Issue] [SQL scripts] Cannot execute query with a value containing a semi-colon - Closed.
- 0001096: [[TA] Issue] [Shortcuts] NullPointerException when there is an empty .macro file - Closed.
- 0001527: [[TA] Issue] [CONVERT INSTRUCTION] : declaring several input and/or output resources in convert instruction doesn't lead to test failure - Closed.
- 0001834: [[TA] Issue] [DbUnit assert contains] Wrong error message when the dataset comparison fails - Closed.
- 0001832: [[TA] Issue] [DbUnit assert equal] Wrong error message when the dataset comparison fails - Closed.
- 0001833: [[TA] Issue] [DbUnit Assert Contains] Modify the diff report content - Closed.
- 0001542: [[TA] Issue] [SCRIPT/TEARDOWN PHASE] : no warning message for assertion error during the teardown phase of a test case - Closed.
- 0001548: [[TA] Issue] [REPOSITORY/URL CATEGORY/HTTP] : loading a resource from a HTTP URL leads to a build success even if the resource  doesn't exist - Closed.
- 0001908: [[TA] Issue] [DbUnit Assert Contains] Improve the diff report - Closed.
- 0002048: [[TA] Issue] [DbUnit Pseudo primary keys] Typo in error message - Closed.
- 0002093: [[TA] Issue] [TargetNotFoundException] Improve error message - Closed.
- 0002083: [[TA] Issue] [FTP client] Typo in error message - Closed.
- 0002042: [[TA] Issue] [FTP plugin] Typo in error message - Closed.
- 0002059: [[TA] Issue] ResourceNotFoundException : Typo in error message - Closed.


Squash TA Server - 1.5.0.RELEASE
================================
- 0001503: [[TA] Issue] [Squash-TA server] The Sahi proxy fails to start when the server path contains a space - Closed.


Squash TA Toolbox - 1.5.0.RELEASE
================================
- 0001326: [[TA] Enhancement] [Squash-TA toolbox] Run configuration to run all the test cases within the selected directory - Closed.
- 0002047: [[TA] Enhancement] [Squash-TA toolbox] Associate .macro files with the text editor in Eclipse - Closed.
- 0001909: [[TA] Enhancement] [Squash-TA-toolbox] Remove soapUI plugin from Eclipse - Closed.
- 0001947: [[TA] Enhancement] [Squash-TA toolbox] Remove quantum DB from Eclipse - Closed.
- 0001534: [[TA] Enhancement] [Squash-TA toolbox] The workspace run configurations should refresh resources upon completion - Closed.
- 0001922: [[TA] Enhancement] [Squash-TA toolbox] Upgrade selenium server to version 2.31.0 - Closed.
- 0001982: [[TA] Enhancement] [Squash-TA toolbox] Upgrade sahi version to v43_20130206 - Closed.
- 0001993: [[TA] Enhancement] [TA toolbox]  Upgrade sahi version to v43_20130206 - Closed.
- 0001932: [[TA] Enhancement] [Squash-TA toolbox] Configure Sahi so it can run multiple tests with FF 13+ - Closed.
- 0001489: [[TA] Issue] [WORKSPACE] : Special characters issue in the workspace path access - Closed.

