
Squash Keyword Framework 1.13.0 Release Note
============================================

Major improvements in this release
----------------------------------

* Add the metadata feature (which handle the Squash TM test case - SKF automated test autolink)
* Upgrade of the Soapui library embedded in the soapui plugin
* Upgrade of the selenium library embedded in the selenium plugin
  * transition to selenium 3
  * creation of a legacy selenium plugin for selenium 1 compatibility

----

**Story**
* SQTA-275 : SKF : Valeur de "tf.metadata.check" entre crochet
* SQTA-137 : Clé - valeur multiligne
* SQTA-136 : clé - valeur mono ligne
* SQTA-135 : Ajout des metadata de type clé seule dans skf
* SQTA-310 : Evolution du rapport contenant les metadata du goal list de skf
* SQTA-277 : SKF : Valeur de "tf.metadata.check.keys" entre crochet
* SQTA-269 : [framework] No METADATA section allowed in an Ecosystem file
* SQTA-232 : All Metadata Keys of a SKF test file must be unique
* SQTA-222 : Analyse d'unicité des valeurs des metadonnées restreinte à certaines clés
* SQTA-221 : Monter soapui en version >= 5.5.0 dans le framework
* SQTA-177 : Ajouter une propriété pour vérifier l'unicité de la valeur d'une métadonée pour une clé donnée sur le projet pour le goal check-metadata
* SQTA-134 : Ajout de la phase metadata dans skf
* SQTA-106 : Ajouter le goal check-metadata
* SQTA-91 : Ajouter les metadata des tests au rapport du goal list du skf
* SQTA-90 : Ajout de la fonctionnalité metadata dans skf
* SQTA-36 : [TA-framework] Upgrade Selenium

**Bug**
* SQTA-309 : Lors d'une exécution "run", message final erroné quand l'outil détecte une phase metadata mal positionnée. 
* SQTA-308 : Le goal check-metadata du skf doit repérer les phases "METADATA :" mal positionnées
* SQTA-300 : CONVERT : Message d'erreur imprécis lorsque le converter n'est pas disponible avec le format demandé
* SQTA-298 : fwk - NullPointerException : EXECUTE get.all WITH $()
* SQTA-292 : SKF - Erreur lorsqu'on exécute du Selenium sans redéclarer les dépendances
* SQTA-87 : Framework fails to identify two Same-signature-with-different-case-character Macros as Duplicate of each other
* SQTA-54 : Erreur de Parsing de DSLs de Squash s'il y a des espaces entre la nature et le nom du Converter
* SQTA-390 : Rapport SKF : Mise en page incorrecte


