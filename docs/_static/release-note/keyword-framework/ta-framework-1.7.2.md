
Squash TA framework - v1.7.2-RELEASE
================================

Enhancement
-----------
3065[TA] Enhancement    [Sahi timeout] Increase Sahi default timeout
3066[TA] Enhancement    [DbUnit] Macro : dbunit assert contains with filter
3077[TA] Enhancement    [TA fwk][Soapui] Create macro for soapui
3089[TA] Enhancement    [TA fwk] Improve failed instruction parsing error log

Issue
-----
2858[TA] Issue          [TA framework] [DB] Connection test used doesn't work with oracle
2972[TA] Issue          Random name collisions in inline ressource may crash test parsing
2838[TA] Issue          [TA fwk][TA URL repository] TA URL repository not working with protocole file
2947[TA] Issue          Temporary "Default" directory in the wrong place
2824[TA] Issue          [Sahi timeout] Improve the logs when timeout occurs during sahi script execution
2675[TA] Issue          Execution of sql script doesn't work with oracle