
Squash Keyword Framework 1.12.0 Release Note
============================================

Major improvements in this release
----------------------------------

* Add the support of junit 5 test scripts in Squash Keyword framework.
  * Summary
    * He can execute junit jupiter (junit5) and junit vintage (junit4) tests
    * Junit 5 annotations support is limited to "@Test" and "@DisplayName"
    * No execution by class supported
  * SKF resource added
    * Bundle : "script.junit5"
    * Result : "result.junit5"
  * SKF engine components added :
    * Converter : CONVERT {Res<script.java>} TO script.junit5(structured) AS {Res<script.junit5>}
    * Command :  This command is parametrized using a mandatory option-formatted FileResource, the content of which can be set in two (mutually exclusive) ways :
      * Use test unique Id : EXECUTE execute WITH {Res<script.junit5>} AS {Res<result.junit5>} USING $(uniqueId:{uniqueId})
      * Use test displayName : EXECUTE execute WITH {Res<script.junit5>} AS {Res<result.junit5>} USING $(qualifiedClassName:{qualifiedClass},displayName:{displayName})
    * Assert :
      * ASSERT {Res<script.junit5>} IS success
  * SKF Macro :
````
# EXECUTE_JUNIT_TEST {displayName} FROM {qualifiedClass} IN {bundlePath}
=>

LOAD {bundlePath} AS junit5{%%r1}.file
CONVERT junit5{%%r1}.file TO script.java(compile) AS junit5{%%r1}.bundle
CONVERT junit5{%%r1}.bundle TO script.junit5(structured) AS junit5{%%r1}.script
EXECUTE execute WITH junit5{%%r1}.script AS junit5{%%1}.result USING $(qualifiedClassName:{qualifiedClass},displayName:{displayName})
ASSERT junit5{%%1}.result IS success
````

````
# EXECUTE_JUNIT_TEST {displayName} FROM {qualifiedClass} IN {bundlePath} WITH COMPILE OPTIONS {options}
=>

LOAD {bundlePath} AS junit5{%%r1}.file
CONVERT junit5{%%r1}.file TO script.java(compile) USING {options} AS junit5{%%r1}.bundle
CONVERT junit5{%%r1}.bundle TO script.junit5(structured) AS junit5{%%r1}.script
EXECUTE execute WITH junit5{%%r1}.script AS junit5{%%1}.result USING $(qualifiedClassName:{qualifiedClass},displayName:{displayName})
ASSERT junit5{%%1}.result IS success
````


----

**Bug**
* SQTA-151 : [CI] La version de SKF utilisée pour les tests non-passants est mise à jour manuellement.
* SQTA-105 : Erreur dans l'execution report : Impossible d'ouvrir la Trace
* SQTA-124 : Erreur de nommage dans les rapports fwk/junit/cucumber


**Story**
* SQTA-45	[Framework] Générer le listing des instructions
* SQTA-19	TA Framework - Support JUnit 5