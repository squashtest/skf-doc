
Squash Keyword Framework 1.14.0 Release Note
============================================

Major improvements in this release
----------------------------------

* Most of the improvements are for the support of the Squash TF Runners

----

**Story**
* SQTA-322 : Créer la documentation pour l'utilisation des métadonnées avec le SKF
* SQTA-386 : Mettre en avant les macros en plus des instructions de base en cas de parsing error dans un fichier .macro ou .ta
* SQTA-389 : Autoriser l'arborescence dans le répertoire shortcuts
* SQTA-501 : Création d'un classLoader qui intègre une logique lors du chargement des classes

**Bug**
* SQTA-385 : Le goal check-metadata doit repérer quand la déclaration de la section metadata est suivie de texte sur la même ligne
* SQTA-404 : Rapport jUnit/SKF : Titre décalé depuis Jenkins
* SQTA-404 : Rapport jUnit/SKF : Titre décalé depuis Jenkins
* SQTA-435 : fwk w/ meta : Logs imprécis lors d'une clé avec valeur "null"
* SQTA-441 : Des fichiers temporaires sont créés et non effacés après chacun de nos builds du SKF
* SQTA-478 : Conformer le rapport HTML simple au modèle du rapport détaillé
* SQTA-502 : Problème de la InitSilencingLoggerFactory sur la classe: org.junit.ComparisonFailure


