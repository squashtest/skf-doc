Squash TA framework - v1.6.0-RELEASE
================================

Enhancement
===========
- 1616	[TA] Enhancement	[Params] Replace placeholders with values from a property file
- 2078	[TA] Enhancement	[squash-ta-engine] Add 'verify' operations
- 2346	[TA] Enhancement	[squash-ta-eclipse-plugin] Missing shebang in the Db target property files created using Eclipse
- 2234	[TA] Enhancement	[squash-ta-engine] Simplify the statuses and the execution lifecycle
- 1999	[TA] Enhancement	Add an exporter to generate HTML execution reports
- 2484	[TA] Enhancement	Add the html report in the Squash TA project archetype
- 2348	[TA] Enhancement	[squash-ta-sahi-plugin] Improve error message when the browser is not properly configured

Issue
=====
- 2250	[TA] Issue	        [sahi-plugin]sahi script execution fails
- 2353	[TA] Issue	        Allow blank lines in property files
- 2259	[TA] Issue	        setValue impossible avec Sahi et Firefox 20
- 2331	[TA] Issue	        [TA][CoreUtils] the FileTree.enumerate() method is too slow for large file trees
- 2221	[TA] Task	        Do some clean up in the code according to our qualimetric standards and add some unitary tests.