
TA 1.11.0 Release Note (DSL & Framework)
========================================


Evolution
---------

* Global

  * Evolution du framework pour gérer l’arrivée du runner squash tf java junit
  * Evolution du framework pour gérer l’arrivée du runner squash tf cucumber java
  * Mise à jour de la license suite au changement d’année

* Plugin xml-function (manipulation xml)

  * Création du plugin XML-functions : possibilité d’appliquer des transformations XSL (aka XSLT) à des ressources de type XML. /!\ Pour ceux utilisant déjà ce plugin avant son intégration dans le framework, il faudra supprimer l’ancienne dépendance lors de la montée de version.

* Plugin men-xml
  
  * Reprise de l’historique de code préalable à l’intégration & Intégration du plugin XML du MEN Orléans. /!\ Pour ceux utilisant déjà ce plugin avant son intégration dans le framework, il faudra supprimer l’ancienne dépendance lors de la montée de version.

* Plugin commons components

  * Spécification de l’encodage utilisé pour la substitution de paramètres
  * Amélioration du compilateur de JavaCodeBundle pour qu’il accepte de prendre un fichier unique en entrée
  * Ajout d’un filtre configurable pour épargner les fichiers binaires dans la macro de substitution de paramètres
  * Ajout de la commande Dump to local filesystem
  * Exploitation des ressources CSV : transformation en XML

* Plugin DB

  * Spécification de l’encodage utilisé pour les requêtes
  * Ajout de clefs de configuration pour changer le séparateur de requête dans les scripts JDBC
  * Ajout de vérifications de validité de dataset à celles de Dbunit
  * Ajout d’une macro pour faire des count BDD
  * Enrichissement du contexte d’échec fonctionnel sur les assertions DB : les données attendues et réelles sont désormais attachées au rapport d’échec du l’assertion de comparaison
  * Ajout de l’assertion « Database not.contains »

* Plugin SoapUI

  * Capture des logs de SoapUI

* Plugin FTP

  * Ajout de la fonctionnalité de traitement des arbres de fichiers au plugin FTP

* Plugin SFTP

  * Ajout de la fonctionnalité de traitement des arbres de fichiers au plugin FTP

* Framework

  * Service mutualisé pour logger de manière normalisée le nom d’un composant.
  * Création du service mutualisé de traitement de la configuration des composants.

Correction
----------

* Plugin DB

  * Résolution d’une fuite de fichier ouvert
  * FIX : régression dans le module DB (tables existantes jamais trouvées)
  * FIX : échec de l’opération DELETE sur HSQLDB

* Plugin SoapUI

  * FIX : incompatibilité des versions 1.10 de Squash TA avec SoapUI community 5.4.0
  * Élimination de la dépendance à jfxrt

* Plugin Filechecker

  * FIX : bug des séquences non réinitialisées dans le filechecker si deux analyses successives

* Framework

  * Résolution partielle d’une race condition entre instances Squash TA sur le stockage de fichiers temporaires s’ils utilisent la configuration par défaut. En cas de survenue du problème, surcharger la propriété système
  * FIX d’une NPE quand Squash TA est exécuté en mode multi module
