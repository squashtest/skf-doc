Squash TA - fwk-1.9.0-RELEASE
=============================

Enhancement
-----------
- 0006056: [[TA] Enhancement] Change slf4j implementation : log4j1.2.17 -> log4j 2.5 (kdrifi) - resolved.
- 0005238: [[TA] Enhancement] Creating target file for SoapUI testing (kdrifi) - resolved.
- 0005605: [[TA] Enhancement] montée de version maven 2.0 -> 3.3.3 (kdrifi) - resolved.
- 0005296: [[TA] Enhancement] Create a project template for Selenium with Squash TA (kdrifi) - resolved.
- 0005611: [[TA] Enhancement] montée de version des librairies techniques (kdrifi) - resolved.
- 0005858: [[TA] Enhancement] montée de version opencsv 2.0 -> 2.3 (kdrifi) - resolved.
- 0005856: [[TA] Enhancement] montée de version librairie freemarker 2.3.19 -> 2.3.23 (kdrifi) - resolved.
- 0005855: [[TA] Enhancement] montée de version librairie apache.logging 1.1.1 -> 1.2 (kdrifi) - resolved.
- 0005853: [[TA] Enhancement] montée de version librairie apache.lang 2.1 -> 2.6 (kdrifi) - resolved.
- 0005852: [[TA] Enhancement] montée de version librairie saxon : 9.1.0.8 -> 9.6.0-7 (kdrifi) - resolved.
- 0005854: [[TA] Enhancement] montée de version librairie apache.io 1.4 -> 2.4 (kdrifi) - resolved.
- 0005859: [[TA] Enhancement] montée de version commons.exec 1.1 -> 1.3 (kdrifi) - resolved.
- 0005857: [[TA] Enhancement] montée de version freemarker jackson (processeur json) 2.3.3 (kdrifi) - resolved.
- 0005608: [[TA] Enhancement] montée de version ftp (apache.commons.net) -> 3.3 (kdrifi) - resolved.
- 0005607: [[TA] Enhancement] montée de version sahi 4.4 -> 5.0 (kdrifi) - resolved.
- 0005606: [[TA] Enhancement] montée de version librairie ssh (sshj) 0.5.0 -> 0.13.0 (kdrifi) - resolved.
- 0005604: [[TA] Enhancement] montée de version Selenium 2.42.2 -> 2.48.2 (kdrifi) - resolved.
- 0005603: [[TA] Enhancement] montée de version SoapUI 4.6.4 -> 5.1.3 (kdrifi) - resolved.
- 0003758: [[TA] Enhancement] [TA fwk][SoapUI] Update embedded SoapUI version (bfranchet) - resolved.
- 0003760: [[TA] Enhancement] [TA framework]Update selenium version (kguilloux) - closed.
- 0002897: [[TA] Enhancement] error message a bit puzzling when wrong java code bundle layout (kguilloux) - closed.
- 0005609: [[TA] Enhancement] montée de version spring 3.0.4 -> 4.2.2 (kdrifi) - resolved.

Issue
-----
- 0005612: [[TA] Issue] Fonction de comparaison dans TestSuiteMacro la méthode "compare" fait échouer la compilation (kdrifi) - resolved.
- 0006152: [[TA] Issue] Debug result exporter broken by the test execution number feature from the new testlist format (edegenetais) - resolved.
- 0001820: [[TA] Issue] [Filechecker] Error message with multiple orthographic errors (kdrifi) - closed.
- 0001808: [[TA] Issue] [Toolbox - Install phase] Odd behavior when trying to unselect the Java package (kdrifi) - closed.
- 0001991: [[TA] Issue] [execution server] Desktop shorcut are not created (kdrifi) - closed.
- 0004214: [[TA] Issue] Shutdown hook leak in the local process library (edegenetais) - resolved.