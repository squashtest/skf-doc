Squash TA framework - v1.7.3-RELEASE
====================================

Enhancement
-----------
- 3107    [TA] Enhancement        A duplicate macro does not cause an error message
- 3161    [TA] Enhancement        [TA framework]Update selenium version
- 3163    [TA] Enhancement        [TA framework]Update sahi version
- 3166    [TA] Enhancement        [squash-ta-plugin-commons-components] Change log level from WARN to INFO when using property squashtest.ta.param.include/exclude
- 3308    [TA] Enhancement        [TA fwk][TA link]Add new configuration properties

Issue
-----
- 2587    [TA] Issue              [Html execution report] A few HTML & CSS tweaks...
- 3316    [TA] Issue              [TA fwk][TA link]An error occur for some status update notification

Task
----
- 3155    [TA] Task               [TA fwk] Update license header