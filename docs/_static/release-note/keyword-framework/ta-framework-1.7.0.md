

/!\ WARNING /!\
In this Squash-TA framework 1.7.0 version we have pushed some evolutions in order to simplify the pom.xml file. As a consequence, 
a new pom.xml template has been introduce (you can find a sample of a pom based on this new template here: 
https://sites.google.com/a/henix.fr/wiki-squash-ta/user/execution-and-reporting/squash-ta-execution#TOC-Squash-TA-default-pom). 
However projects based on the previous pom template still working. These evolutions leads also to some modifications 
in the Squash-TA toolbox 1.7.0 (change in run-configuration) and Squash-TA server 1.6.0. Have a look on the compatibility matrices below.

We also decide, for the sake of consistency, to use "squash-ta:run" goal as our default execution command in our two tools : Squash-TA toolbox and Squash-TA server.
Indeed in previous version of Squash-TA toolbox, the "run configuration" used the "integration-test" maven lifecycle to execute test. The table below summarizes the 
changes:
+---------------------+-----------------------+-----------------------------------------------+
| Command             | TA tbx 1.6.0          | TA tbx 1.7.0                                  |
+---------------------+-----------------------+-----------------------------------------------+
| integration-test    | Run all tests         | (Before TA fwk 1.7.0) Run all tests           |
+---------------------+-----------------------+-----------------------------------------------+
| integration-test    | Run selected test(s)  | (Before TA fwk 1.7.0) Run selected test(s)    |
+---------------------+-----------------------+-----------------------------------------------+
| integration-test    | Run test list	      | (Before TA fwk 1.7.0) Run test list           |
+---------------------+-----------------------+-----------------------------------------------+
| squash-ta:run       |                       | Run all tests                                 |
+---------------------+-----------------------+-----------------------------------------------+
| squash-ta:run       |                       | Run selected test(s)	                      |
+---------------------+-----------------------+-----------------------------------------------+
| squash-ta:run       |                       | Run test list	                              |
+---------------------+-----------------------+-----------------------------------------------+

To upgrade a TA project to Squash-TA framework 1.7.0 you have, as usual, to upgrade the framework version in the pom.xml of your project (link). You could also decide to modify your pom
in order to it was based on the new pom template.
Step to migrate your pom to new template
    1) Have a look on the compatibility matrices below
    2) Take as base the sample pom that you can find in our wiki : 
        https://sites.google.com/a/henix.fr/wiki-squash-ta/user/execution-and-reporting/squash-ta-execution#TOC-Squash-TA-default-pom
    3) Replace the groupID, ArtifactID and the version of this sample by those currently used in your pom
    4) Modify the property logConfFile if needed
    5) If you have some jdbc driver define in your current pom, declare them also in the new pom
    6) Transfer all specific configuration you've done in your current pom to the new pom
    7) You new pom is ready

    
Here are the compatibility matrices :

Compatibility between the Squash-TA framework and the pom.xml
+-----------+-----------+---------------+
| TA fwk	| pom.xml   | Compatibility |
+-----------+-----------+---------------+
| <= 1.6.0	| old	    | OK            |
+-----------+-----------+---------------+
| <= 1.6.0	| new	    | => KO <=      |
+-----------+-----------+---------------+
| 1.7.0	    | old	    | OK            |
+-----------+-----------+---------------+
| 1.7.0	    | new	    | OK            |
+-----------+-----------+---------------+

Compatibility between the Squash-TA framework the pom.xml and the Squash-TA toolbox
+-----------+-----------+-----------+---------------+
| TA fwk	| pom	    | TA tbx    | Compatibility |
+-----------+-----------+-----------+---------------+
| <= 1.6.0	| old	    | <= 1.6.0  | OK            |
+-----------+-----------+-----------+---------------+
| 1.7.0     | old	    | <= 1.6.0  | OK            |
+-----------+-----------+-----------+---------------+
| <= 1.6.0	| old	    | 1.7.0     | /!\ (1)       |
+-----------+-----------+-----------+---------------+
| 1.7.0	    | old	    | 1.7.0     | OK            |
+-----------+-----------+-----------+---------------+
| 1.7.0	    | new	    | 1.7.0     | OK            |
+-----------+-----------+-----------+---------------+
(1) Use the "run configuration" prefixed with "(Before TA fwk 1.7.0)"

Compatibility between the Squash-TA framework, the pom.xml and the Squash-TA server
+-----------+-----------+-----------+---------------+
| TA fwk	| pom	    | TA srv    | Compatibility |
+-----------+-----------+-----------+---------------+
| <= 1.6.0	| old	    | <= 1.5.0  | OK            |
+-----------+-----------+-----------+---------------+
| 1.7.0     | old	    | <= 1.5.0  | OK            |
+-----------+-----------+-----------+---------------+
| 1.7.0     | new	    | <= 1.5.0  | /!\ (2)       |
+-----------+-----------+-----------+---------------+
| <= 1.6.0	| old	    | 1.6.0     | OK            |
+-----------+-----------+-----------+---------------+
| 1.7.0	    | old	    | 1.6.0     | OK            |
+-----------+-----------+-----------+---------------+
| 1.7.0	    | new	    | 1.6.0     | OK            |
+-----------+-----------+-----------+---------------+
/!\ (2) Problem with the attachment for the junit report in jenkins (doesn't break the build)
You have to modify your mvnsetting.xml (file located in ${TA-SRV_INSTALL_DIR\execution_home}) to restore the correct behahior for all your jenkins job :
* Add inside the <profiles> tag (near the end of the file):
    <profile>
      <id>jenkins-attachment-mode</id>
      <properties>
        <ta.jenkins.attachment.mode>true</ta.jenkins.attachment.mode>
      </properties>
    </profile>
* Add below the <profiles> tag (near the end of the file):
    <activeProfiles>
      <activeProfile>jenkins-attachment-mode</activeProfile>
    </activeProfiles>


    

		
Squash-TA Toolbox - v1.7.0-RELEASE
================================
- 2681	[TA] Enhancement	[TA toolbox] Create new run conf

Squash-TA Server - v1.6.0-RELEASE
================================
- 2684	[TA] Enhancement	[TA server][Job template] Add the new "ta.jenkins.attachment.mode" parameter

Squash-TA framework - v1.7.0-RELEASE
================================
- 1558	[TA] Enhancement	Execute a process locally
- 2679	[TA] Enhancement	[DbUnit] Implement DbUnit "escapePattern" property in order to escape reserved SQL keywords
- 2566	[TA] Enhancement	[Selenium-plugin] The Selenium surefire reports should be displayed in the JUnit view in Eclipse
- 2602	[TA] Enhancement	Using a Placeholder on a Zip file corrupt it
- 2352	[TA] Enhancement	[squash-ta-maven-plugin] Split and shorten the error messages
- 1988	[TA] Enhancement	[squash-ta-soapui-plugin] Execute SoapUI bundles
- 2506	[TA] Enhancement	Only initialize needed target
- 2404	[TA] Enhancement	Include script comment in the report
- 2143	[TA] Enhancement	Change error message when error in TEARDOWN phase
- 1923	[TA] Enhancement	[Relative date converter] Specify the locale
- 1986	[TA] Enhancement	[squash-ta-sahi-plugin] Add a timeout to sahi commands
- 2080	[TA] Enhancement	Create the temporary files into a specific directory
- 2508	[TA] Enhancement	Simplify the maven configuration (pom) for project using our plugin
- 2347	[TA] Enhancement	[squash-ta-project-archetype] Use a maven property to define the squash-ta version in the pom file
- 2507	[TA] Enhancement	Report Target initialization result in html report
- 2604	[TA] Enhancement	[Placeholder]  there is no warning message if we don't give properties to the convert instruction
- 2532	[TA] Enhancement	[HTML report] Create a HTML builder
- 2591	[TA] Enhancement	[FTPTargetCreator] Improve the logs when a FTP target cannot be instantiated
- 2068	[TA] Enhancement	[DSL parser] Check whether the category is missing in a clause
- 2217	[TA] Issue			[squash-ta-plugin-soapui] Testcase successful when trying to run a testcase from the wrong testsuite
- 2693	[TA] Issue			[Soap UI] Failure when a test is in a directory containing a space
- 2690	[TA] Issue			[FTP Target] Host property missing from target configuration
- 2590	[TA] Issue			[DbTargetCreator] Wrong message in the logs
- 2607	[TA] Issue			[DbUnit] [Assert contain] Ambiguous error message
- 2429	[TA] Issue			[Converter] Java heap space in CONVERT file TO file(param)
- 2392	[TA] Issue			[Selenium] Selenium server OFF => Success instead of error
- 2612	[TA] Issue			[log command] Improve error message when trying to write an empty message in the log
- 2111	[TA] Issue			The test fails with a erroneous message when there is a comment at the end of a phase tag  (on the same line)
- 2476	[TA] Issue			[html-report] tests not run are broken links
- 1904	[TA] Issue			Squash-TA fails to delete some of the temporary files
- 2252	[TA] Issue			[macro] use of parenthesis in SQL statement inline resource failed
- 2674	[TA] Issue			Bad macro initialisation