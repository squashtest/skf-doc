Squash TA - tbx-1.9.0-RELEASE
=============================
- 0006059: [[TA] Enhancement] Upgrade eclipse version to Mars (kdrifi) - resolved.
- 0006060: [[TA] Enhancement] Change launch configuration of embedded eclipse for log4j 2.5 compatibility. (kdrifi) - resolved.
- 0006058: [[TA] Enhancement] Patch maven embedded version 3.3.3 for tbx. (kdrifi) - resolved.
- 0005843: [[TA] Enhancement] Upgrade Tomcat version 6.0.35 -> 8.0 tbx. (kdrifi) - resolved.
- 0005839: [[TA] Enhancement] Upgrade maven version 3.0.4 -> 3.3.3 tbx. (kdrifi) - resolved.
- 0005840: [[TA] Enhancement] Upgrade Jailer version 4.0.6 -> 5.1 tbx. (kdrifi) - resolved.
- 0005841: [[TA] Enhancement] Upgrade Sahi version 4.4 -> 5.0 tbx. (kdrifi) - resolved.
- 0005842: [[TA] Enhancement] Upgrade Selenium version 2.42.2 -> 2.49.0 tbx. (kdrifi) - resolved.
- 0005377: [[TA] Enhancement] Updating java version : 1.6 -> 1.7 of Squash-TA toolbox (kdrifi) - resolved.