
Squash TA eclipse plugin - 1.0.0
==========================================

Enhancement
-----------
- 0001544: [Enhancement] Create a new target wizards to create configurations the two most used target types: database and web
- 0001540: [Enhancement] Create a Squash TA perspective to organise Squash TA functionalities in the toolbox Eclipse instance
- 0001539: [Enhancement] Create a Squash TA plugin to offer Squash TA enhanced functionalities

Task
----
- 0001772: [Task] Integrate the TA eclipse plugin in the toolbox
- 0001756: [Task] Industrialize eclipse plugin
