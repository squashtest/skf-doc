
Squash TF IntelliJ IDEA plugin 0.0.2 Release Note
=================================================

**Story**
* SQTA-397 : Faire évoluer la liste des templates d'instructions proposées par intellij
* SQTA-332 : The hook must be the first line of the shortcut file.
* SQTA-331 : Accept Value List as input for USING keyword
* SQTA-273 : [plugin intelliJ] Error annotation will be raised if a Metadata is found in an Ecosystem file
* SQTA-272 : [plugin IntelliJ] Autocompletion for Metadata section in a SKF project
* SQTA-270 : [plugin IntelliJ] Syntax checking for Metadata KEY and VALUE
* SQTA-264 : Autoriser les espaces entre le type cible de la ressource et nom du converter
* SQTA-139 : Prise en charge des metadata du skf par le plugin intellij
* SQTA-338 : In USING value list, a "comma" must NOT be followed by a empty or only-space/tabulation value
* SQTA-349 : [plugin intelliJ] A keyword which is not all in uppercase blocks the engine component validation
* SQTA-343 : [plugin IntelliJ] placeholder {%%whatever} to create a UNIQUE_NUMBER_VALUE
* SQTA-336 : Empty autocompletion with tooltips for value position in DSL basic instruction

**Task**
* SQTA-287 : Vérifier que plugin intelliJ en version 0.0.2.IT3 ne dépend PAS d'une installation locale de maven
* SQTA-285 : Ajouter le déploiement continu de la documentation du plugin intelliJ
* SQTA-271 : Mettre en place la notification qui avertit l'utilisateur que les données de composants SKF sont disponibles
* SQTA-249 : Restructurer le code du plugin intelliJ

**Bug**
* SQTA-426 : Pas de complétion dans les macro avant des commentaires
* SQTA-412 : IntelliJ : Autocomplétion sur ASSERT : IDE Fatal Error
* SQTA-398 : Supprimer le retour lors de la selection d'un premier token d'instruction via l’auto complétion
* SQTA-395 : [IntelliJ plugin] Macro signature wrongly interpreted
* SQTA-304 : IntelliJ - Color scheme : les macros du .ta sont liés au macro symbol du Squash Macro File
* SQTA-295 : IntelliJ - EXECUTE : Le plugin n'accepte pas plusieurs définitions en inline dans la même ligne
* SQTA-294 : IntelliJ - KEYWORD : Erreur lorsque la ressource est définie en inlined
* SQTA-293 : IntelliJ - EXECUTE : Erreur lorsque le ON n'est pas présent
* SQTA-290 : Plugin IntelliJ - Failed to extract component registry data / load SKF component data : galaxia-component-probe:jar:1.1.0.IT3 [is missing] in fact is mistakenly referenced (should be 1.0.1.IT3)
* SQTA-196 : IntelliJ - Template incorrect - Mauvais ordre des informations
* SQTA-193 : IntelliJ - Autocomplétion sensible à la casse
* SQTA-186 : Le plugin IntelliJ affiche ses erreurs par des annotations de fichiers, des infobulles idea seraient préférables.
* SQTA-185 : Pas de reporting d'erreur sur la construction des données de composants dans le plugin intelliJ
* SQTA-182 : IDE error dans le cas de certains pom.xml sur IntelliJ
* SQTA-175 : Erreur affichée par IntelliJ lors d'un DEFINE avec une requête SQL contenant des sous-requêtes
* SQTA-377 : IntelliJ - RAF : Sensibilité à la casse
* SQTA-373 : Corriger le user manuel intellij sur la partie "Squash TF Run"
* SQTA-351 : IntelliJ CONVERT - Erreur losqu'on ne met pas de () après le type de ressource cible
* SQTA-350 : IntelliJ - Erreur losque 2 macros ne sont séparées que par un saut de ligne
* SQTA-184 : IntelliJ : Erreur indiquée dans un EXECUTE avec une liste incluant $(:)


