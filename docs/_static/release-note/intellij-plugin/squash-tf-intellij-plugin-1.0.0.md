
Squash TF IntelliJ IDEA plugin 1.0.0 Release Note
=================================================

**Story**
* SQTA-306 : IntelliJ - Commande erronée : Rendre le message plus user-friendly
* SQTA-318 : Auto-completion for Squash DSL instruction line needs to be reviewed
* SQTA-332 : The hook must be the first line of the shortcut file.
* SQTA-408 : Update DSL parsers and lexers for a better error management, for the autocompletion enhancement and also for bug fix
* SQTA-409 : Syntax Validation for Phase and Basic instruction needs to be reviewed
* SQTA-413 : create a weak warning for empty value of type "{}" or "{{}}"
* SQTA-417 : Wrong keyword blocks or not the auto-completion of others keyword
* SQTA-416 : Move syntax validation mechanism for Squash Instruction value from PARSING to JAVA level
* SQTA-427 : No difference in style between a built-in Macro and a custom-defined macro

**Task**
* SQTA-367 : Mise à jour de la doc d'installation pour le dev intellij

**Bug**
* SQTA-194 : IntelliJ - Autocomplétion ne fonctionne pas lorsqu'on a déjà écrit une lettre puis ctrl+space
* SQTA-301 : IntelliJ - Une ligne en erreur au niveau de parseur peut bloquer l'affichage des lignes suivantes erronées
* SQTA-325 : IntelliJ - Autocomplétion de phase : Retour à la ligne en conservant les premières lettres indiquées
* SQTA-176 : Erreur affichée par IntelliJ lorsqu'une phase constitue la dernière ligne du script
* SQTA-305 : IntelliJ - Premier token erroné : Pas de coloration après correction
* SQTA-410 : IntelliJ : Phase suivie d'une instruction (même ligne) accepté par le plugin
* SQTA-433 : IntelliJ : most -> must
* SQTA-434 : IntelliJ : Pas d'autocomplétion sur les user-defined macro en minuscules
* SQTA-453 : IntelliJ - Autoriser l'arborescence dans le répertoire shortcuts