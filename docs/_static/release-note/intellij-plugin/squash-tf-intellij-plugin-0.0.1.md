
Squash TF IntelliJ IDEA plugin 0.0.1 Release Note
=================================================


This IntelliJ IDEA plugin brings to IntelliJ IDEA ide the support of project written with Squash Keyword Framework (previously known as Squash TA framework)
The features brought of this plugin are : 
* syntax highlighting for .ta and .macro
* syntax validation for .ta and .macro
* completion for .ta and .macro file

It's the first iteration of this plugin. You can find the doc here : https://squash-tf.readthedocs.io/projects/intellij-plugin

----

**Bug**
* SQTA-67 : Vérification trop restrictive de l'emplacement des scripts TA
* SQTA-79 : Le plugin intelliJ ne peut extraire les données de composants du moteur sous Windows
* SQTA-93 : Freeze de intellij + Timeout à la 1ere exécution du plugin
* SQTA-94 : Améliorer la gestion et la remontée d'erreur dans intellij
* SQTA-169 : Erreur sur les setup.ta/teardown.ta avec le plugin IntelliJ
* SQTA-192 : Plusieurs projets ouvert dont 1 en erreur -> Tout mis en erreur

**Story**
* SQTA-11 : Studio de développement DSL sur IntelliJ - Etude préalable
* SQTA-35 : [Plugin InetlliJ] Industrialisation